﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Codings.Interfaces
{
    public interface ICode
    {
        string Code(string stringToCode);
        string Decode(string stringToDecode);
    }
}
