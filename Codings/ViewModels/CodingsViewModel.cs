﻿using Codings.Helpers;
using System.ComponentModel;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Windows.Input;

namespace Codings
{
    public class CodingsViewModel : INotifyPropertyChanged
    {
        #region Commands
            #region PrivateCommandFields
            private ICommand _codeShannonCommand;
            private ICommand _decodeShannonCommand;
            private ICommand _codeHammingCommand;
            private ICommand _decodeHammingCommand;
            #endregion

            #region CommandProperties
            public ICommand CodeShannonCommand
            {
                get
                {
                    if (_codeShannonCommand == null)
                    {
                        _codeShannonCommand = new RelayCommand(
                            param => CodeShannon(),
                            param => true
                        );
                    }
                    return _codeShannonCommand;
                }
            }

            public ICommand DecodeShannonCommand
            {
                get
                {
                    if (_decodeShannonCommand == null)
                    {
                        _decodeShannonCommand = new RelayCommand(
                            param => DecodeShannon(),
                            param => true
                        );
                    }
                    return _decodeShannonCommand;
                }
            }

            public ICommand CodeHammingCommand
            {
                get
                {
                    if (_codeHammingCommand == null)
                    {
                        _codeHammingCommand = new RelayCommand(
                            param => CodeHamming(),
                            param => true
                        );
                    }
                    return _codeHammingCommand;
                }
            }

            public ICommand DecodeHammingCommand
            {
                get
                {
                    if (_decodeHammingCommand == null)
                    {
                        _decodeHammingCommand = new RelayCommand(
                            param => DecodeHamming(),
                            param => true
                        );
                    }
                    return _decodeHammingCommand;
                }
            }
        #endregion
        #endregion

        #region INotifyPropertyMembers
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
        #endregion

        #region AppPrivateFields
        private string _shannonInputTextBoxValue;
        private string _shannonOutputTextBoxValue;
        private string _hammingInputTextBoxValue;
        private string _hammingOutputTextBoxValue;
        private string _entropyValue;
        private string _avgCodeWordValue;
        private string _codeEfficiency;

        private ShannonFano _shannon;
        private Hamming _hamming;
        #endregion

        #region AppProperties
        public string ShannonInputTextBoxValue
        {
            get => _shannonInputTextBoxValue;
            set
            {
                _shannonInputTextBoxValue = value;
                OnPropertyChanged(MethodBase.GetCurrentMethod().Name.Substring(4));
            }
        }
        public string ShannonOutputTextBoxValue
        {
            get => _shannonOutputTextBoxValue;
            set
            {
                _shannonOutputTextBoxValue = value;
                OnPropertyChanged(MethodBase.GetCurrentMethod().Name.Substring(4));
            }
        }
        public string HammingInputTextBoxValue
        {
            get => _hammingInputTextBoxValue;
            set
            {
                _hammingInputTextBoxValue = value;
                OnPropertyChanged(MethodBase.GetCurrentMethod().Name.Substring(4));
            }
        }
        public string HammingOutputTextBoxValue
        {
            get => _hammingOutputTextBoxValue;
            set
            {
                _hammingOutputTextBoxValue = value;
                OnPropertyChanged(MethodBase.GetCurrentMethod().Name.Substring(4));
            }
        }
        public string EntropyValue
        {
            get => _entropyValue;
            set
            {
                _entropyValue = value;
                OnPropertyChanged(MethodBase.GetCurrentMethod().Name.Substring(4));
            }
        }
        public string AverageCodeWordLengthValue
        {
            get => _avgCodeWordValue;
            set
            {
                _avgCodeWordValue = value;
                OnPropertyChanged(MethodBase.GetCurrentMethod().Name.Substring(4));
            }

        }
        public string CodeEfficiencyValue
        {
            get => _codeEfficiency;
            set
            {
                _codeEfficiency = value;
                OnPropertyChanged(MethodBase.GetCurrentMethod().Name.Substring(4));
            }
        }
        #endregion

        #region Constructors
        public CodingsViewModel()
        {
            _shannon = new ShannonFano();
            _hamming = new Hamming();
        }
        #endregion

        #region AppMethods
        public void CodeShannon()
        {
            _shannon = new ShannonFano();
            if (!ShannonInputTextBoxValue.Equals(string.Empty))
            {
                ShannonOutputTextBoxValue = _shannon.Code(ShannonInputTextBoxValue);
                EntropyValue = _shannon.Entropy.ToString();
                AverageCodeWordLengthValue = _shannon.AverageCodeWordLength.ToString();
                CodeEfficiencyValue = (_shannon.CodeEfficiency * 100).ToString() +"%";
            }
            else
            {
                ShannonOutputTextBoxValue = string.Empty;
                EntropyValue = string.Empty;
                AverageCodeWordLengthValue = string.Empty;
                CodeEfficiencyValue = string.Empty;
            }
        }

        public void DecodeShannon()
        {
            _shannon = new ShannonFano();
            if (!ShannonInputTextBoxValue.Equals(string.Empty))
            {
                ShannonOutputTextBoxValue = _shannon.Decode(ShannonInputTextBoxValue);
                EntropyValue = _shannon.Entropy.ToString();
                AverageCodeWordLengthValue = _shannon.AverageCodeWordLength.ToString();
                CodeEfficiencyValue = (_shannon.CodeEfficiency * 100).ToString() + "%";
            }
            else
            {
                ShannonOutputTextBoxValue = string.Empty;
                EntropyValue = string.Empty;
                AverageCodeWordLengthValue = string.Empty;
                CodeEfficiencyValue = string.Empty;
            }
        }

        public void CodeHamming()
        {
            _hamming = new Hamming();           
            if (!HammingInputTextBoxValue.Equals(string.Empty))
            {
                HammingOutputTextBoxValue = _hamming.Code(HammingInputTextBoxValue);
            }
            else
            {
                HammingOutputTextBoxValue = string.Empty;
            }
            
        }

        public void DecodeHamming()
        {
            _hamming = new Hamming();
            if (!HammingInputTextBoxValue.Equals(string.Empty))
            {
                HammingOutputTextBoxValue = _hamming.Decode(HammingInputTextBoxValue);
            }
            else
            {
                HammingOutputTextBoxValue = string.Empty;
            }          
        }
        #endregion
    }
}
