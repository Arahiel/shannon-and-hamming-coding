﻿using Codings.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Codings.Helpers
{
    public class ShannonFano : ICode
    {
        private int _allOccurences;
        List<Symbol> symbolList;
        public double Entropy;
        public double AverageCodeWordLength;
        public double CodeEfficiency;

        public string Code(string stringToCode)
        {
            CountOccurencesForSymbols(stringToCode);
            CalculateProbability();
            symbolList = symbolList.OrderBy(x => x.Probability).ToList();
            CodeSymbols(symbolList);
            var codedString = CodeInputString(stringToCode);
            Entropy = CalculateEntropy();
            AverageCodeWordLength = CalculateAverageCodeWordLengthValue();
            CodeEfficiency = CalculateCodeEfficiency();
            return GetCodedStringWithAlphabet(codedString);
        }


        public string Decode(string stringToDecode)
        {
            try
            {
                symbolList = new List<Symbol>();
                string alphabet = stringToDecode.Split((char)11)[0];
                string codedString = stringToDecode.Split((char)11)[1];
                var codedSymbols = alphabet.Split((char)30);
                symbolList.AddRange(codedSymbols.Select(x => new Symbol(x.Split((char)29)[0].First(), x.Split((char)29)[1].ToString())));
                string decodedString = string.Empty;
                string temp = string.Empty;
                foreach (var c in codedString)
                {
                    temp += c;
                    try
                    {
                        var associatedSymbol = symbolList.Single(x => x.SymbolCode.Equals(temp));
                        decodedString += associatedSymbol.SymbolCharacter;
                        temp = string.Empty;
                    }
                    catch { }
                }
                CountOccurencesForSymbols(decodedString);
                CalculateProbability();
                Entropy = CalculateEntropy();
                AverageCodeWordLength = CalculateAverageCodeWordLengthValue();
                CodeEfficiency = CalculateCodeEfficiency();
                return decodedString;
            }
            catch
            {
                return "Brak poprawnie zapisanego alfabetu w łańcuchu wejściowym! Jeśli masz problem z poprawnym zapisem, wklej przykładowy zakodowany tekst do notepad++, " +
                    "lub innej aplikacji, która pokaże białe separujące znaki w tekście i użyj ich w ten sam sposób.\n\n" +
                    "<symbol>(GS)<kod>(RS)...<symbol>(GS)<kod>(VT). GS - group separator (ASCII - 29), RS - record separator (ASCII - 30), " +
                    "VT - vertical tab (ASCII - 11).";
            }
        }

        private void CountOccurencesForSymbols(string inputString)
        {
            _allOccurences = inputString.Length;

            var distinctSymbolsInInputString = inputString.Distinct();
            if (symbolList == null)
            {
                symbolList = distinctSymbolsInInputString.Select(x => new Symbol(x)).ToList();
            }
            symbolList.ForEach(x => x.Occurences = inputString.Count(y => y.Equals(x.SymbolCharacter)));
        }

        private void CalculateProbability()
        {
            symbolList.ForEach(x => x.Probability = (double)x.Occurences / _allOccurences);
        }

        private void CodeSymbols(List<Symbol> listOfSymbolsToCode)
        {
            var probabilityHalf = listOfSymbolsToCode.Sum(x => x.Probability) / 2;
            var groups = GetHalfProbabilityGroups(listOfSymbolsToCode, probabilityHalf);
            var firstGroup = groups.Item1;
            var secondGroup = groups.Item2;

            foreach(var symbol in firstGroup)
            {
                symbol.SymbolCodeList.Add(0);
            }
            foreach (var symbol in secondGroup) 
            {
                symbol.SymbolCodeList.Add(1);
            }

            if(firstGroup.Count > 1)
            {
                CodeSymbols(firstGroup);
            }

            if (secondGroup.Count > 1)
            {
                CodeSymbols(secondGroup);
            }
        }

        private Tuple<List<Symbol>, List<Symbol>> GetHalfProbabilityGroups(List<Symbol> listOfSymbolsToCode, double probabilityHalf)
        {
            List<Symbol> firstGroup;
            List<Symbol> secondGroup;
            double tempProbabilitySum = 0.0;
            double oldProbabilitySum;
            int splitIndex = 0;


            foreach (var symbol in listOfSymbolsToCode)
            {
                oldProbabilitySum = tempProbabilitySum;
                tempProbabilitySum += symbol.Probability;

                if(tempProbabilitySum >= probabilityHalf)
                {
                    if(Math.Abs(probabilityHalf - tempProbabilitySum) > Math.Abs(probabilityHalf - oldProbabilitySum))
                    {
                        splitIndex = listOfSymbolsToCode.IndexOf(symbol) - 1;
                    }
                    else
                    {
                        splitIndex = listOfSymbolsToCode.IndexOf(symbol);
                    }
                    break;
                }
            }

            firstGroup = listOfSymbolsToCode.Take(splitIndex + 1).ToList();
            secondGroup = listOfSymbolsToCode.Skip(splitIndex + 1).ToList();
            return Tuple.Create(firstGroup, secondGroup);
        }

        private string CodeInputString(string stringToCode)
        {
            string outputString = string.Empty;

            foreach(var character in stringToCode)
            {
                outputString += symbolList.Single(x => x.SymbolCharacter.Equals(character)).SymbolCode;
            }

            return outputString;
        }

        private double CalculateEntropy()
        {
            return symbolList.Sum(x => x.Probability * x.InformationAmount);
        }

        private double CalculateCodeEfficiency()
        {
            return Math.Round(Entropy / AverageCodeWordLength, 4);
        }

        private double CalculateAverageCodeWordLengthValue()
        {
            return symbolList.Sum(x => x.Probability * x.SymbolCode.Length);
        }

        private string GetCodedStringWithAlphabet(string codedString)
        {
            string alphabet = string.Empty;
            foreach(var symbol in symbolList)
            {
                alphabet += $"{symbol.SymbolCharacter}{(char)29}{symbol.SymbolCode}{(char)30}"; // 29 - group separator, 30 - record separator
            }
            alphabet = alphabet.Remove(alphabet.LastIndexOf((char)30));
            alphabet += (char)11; // 11 - vertical tab

            return codedString.Insert(0, alphabet);
        }
    }
}
