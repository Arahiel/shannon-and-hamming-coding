﻿using System;
using System.Collections.Generic;

namespace Codings.Helpers
{
    public class Symbol
    {
        #region PrivateFields
        private double _probability;
        private string _symbolCode;
        #endregion

        #region PublicProperties
        public char SymbolCharacter { get; set; }
        public string SymbolCode
        {
            get
            {
                if(_symbolCode == null)
                {
                string code = string.Empty;
                foreach (var value in SymbolCodeList)
                {
                    if (value == 1)
                    {
                        code += "1";
                    }
                    else
                    {
                        code += "0";
                    }
                }
                return code;
                }
                else
                {
                    return _symbolCode;
                }
            }
            set
            {
                _symbolCode = value;
            }
        }
        public List<int> SymbolCodeList = new List<int>();
        public int Occurences { get; set; }
        public double Probability
        {
            get => _probability;
            set
            {
                _probability = value;
                InformationAmount = Math.Log(1 / _probability, NumberSystem);
            }
        }
        public double InformationAmount { get; private set; }
        public uint NumberSystem { get; set; } = 2;
        #endregion

        #region Constructors
        public Symbol(char symbol)
        {
            SymbolCharacter = symbol;
        }

        public Symbol(char symbol, uint numberSystem)
        {
            SymbolCharacter = symbol;
            NumberSystem = numberSystem;
        }

        public Symbol(char symbol, string symbolCode)
        {
            SymbolCharacter = symbol;
            SymbolCode = symbolCode;
        }
        #endregion
    }
}
