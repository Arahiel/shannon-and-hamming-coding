﻿using Codings.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Codings.Helpers
{
    public class Hamming : ICode
    {
        private List<int> _bitList = new List<int>();
        private int[,] parityMatrix;

        public string Code(string stringToCode)
        {
            if(CheckIfInputStringIsInproperlyFormatted(stringToCode))
            {
                return "Złe dane wejściowe! Dane wejściowe muszą się składać wyłącznie z cyfr 0 lub 1.";
            }
            FillListWithDataAndParityBits(stringToCode);
            CalculateParityBits();
            return string.Concat(_bitList.SelectMany(x => x == 0 ? "0" : "1"));
        }

        public string Decode(string stringToDecode)
        {
            string outputString = stringToDecode;
            if (CheckIfInputStringIsInproperlyFormatted(stringToDecode))
            {
                return "Złe dane wejściowe! Dane wejściowe muszą się składać wyłącznie z cyfr 0 lub 1.";
            }
            int rowsCount = GetNumberOfParityBits(stringToDecode.Length);
            int columnsCount = stringToDecode.Length;

            CalculateParityMatrix(rowsCount, columnsCount);
            int exitCode = CheckInputForErrors(stringToDecode, rowsCount, columnsCount);
            if(exitCode != 0)
            {
                outputString = CorrectInputBits(stringToDecode, exitCode - 1) + $" - Poprawiono bit na pozycji: {exitCode}"; // -1 ponieważ liczymy pozycje od 0
            }
            return outputString;
        }

        private void CalculateParityMatrix(int rowsCount, int columnsCount)
        {
            parityMatrix = new int[rowsCount, columnsCount];
            for (int i = 0; i < rowsCount; i++)
            {
                int parityBitPosition = (int)Math.Pow(2, i) - 1; // -1 because of counting from 0
                var parityCheckPositionsList = GetParityCheckPositions(columnsCount, parityBitPosition);

                parityMatrix[i, parityBitPosition] = 1;
                foreach (var pos in parityCheckPositionsList)
                {
                    parityMatrix[i, pos] = 1;
                }
            }

        }


        private int CheckInputForErrors(string stringToDecode, int rowsCount, int columnsCount)
        {
            List<int> inputVector= stringToDecode.Select(x => x.Equals('0') ? 0 : 1).ToList();
            int[] outputVector = new int[rowsCount];
            for (int i = 0; i < rowsCount; i++)
            {
                int sum = 0;
                for (int j = 0; j < columnsCount; j++)
                {
                    sum += parityMatrix[i, j] * inputVector[j];
                }
                outputVector[i] = sum % 2;
            }
            var vectorString = string.Concat(outputVector.Reverse().SelectMany(x => x == 0 ? "0" : "1")); // BigEndian
            return Convert.ToInt32(vectorString, 2);
        }

        private string CorrectInputBits(string stringToDecode, int bitPosition)
        {
            var negatedBit = stringToDecode.ElementAt(bitPosition).Equals('0') ? '1' : '0';
            return stringToDecode.Remove(bitPosition, 1).Insert(bitPosition, negatedBit.ToString());
        }

        private bool CheckIfInputStringIsInproperlyFormatted(string input)
        {
            Regex nonBits = new Regex("[^0-1]");
            return nonBits.IsMatch(input);
        }

        private void CalculateParityBits()
        {
            int count = _bitList.Count;
            for (int k = 0; k < count; k++)
            {
                if(_bitList[k].Equals(-1))
                {
                    var parityCheckPositionsList = GetParityCheckPositions(count, k);
                    int sum = 0;
                    foreach(var pos in parityCheckPositionsList)
                    {
                        sum += _bitList[pos];
                    }
                    _bitList[k] = sum % 2;
                }
            }
        }

        private List<int> GetParityCheckPositions(int bitListCount, int bitPosition)
        {
            List<int> parityCheckPositions = new List<int>();
            int counter = 1;
            bool exception = bitPosition == 0 ? true : false;
            bool ifParityCheckPosition = true;
            for (int i = bitPosition + 1; i < bitListCount; i++)
            {
                if(exception)
                {
                    exception = false;
                    counter = 0;
                    continue;
                }

                if (ifParityCheckPosition)
                {
                    parityCheckPositions.Add(i);
                }
                counter++;
                if (counter == bitPosition + 1)
                {
                    ifParityCheckPosition = !ifParityCheckPosition;
                    counter = 0;
                }
            }           
            return parityCheckPositions;
        }

        private void FillListWithDataAndParityBits(string stringToCode)
        {
            foreach (var bit in stringToCode)
            {
                _bitList.Add(bit.Equals('0') ? 0 : 1);
            }
            for (int i = 0; i < _bitList.Count; i++)
            {
                if (IsPowerOfTwo(i + 1))
                {
                    _bitList.Insert(i, -1);
                }
            }
        }

        private bool IsPowerOfTwo(int input)
        {
            return (input & input - 1) == 0;
        }

        private int GetNumberOfParityBits(int numberOfInputBits)
        {
            return (int)Math.Ceiling(Math.Log(numberOfInputBits, 2));
        }
    }
}
